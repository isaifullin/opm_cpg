include conf_gcc.mk

INCLUDEFLAGS = -I./src \
				-I./src -I./src/opm -I./src/opm/grid -I./src/pybind11 -I./thirdparty/pybind11/include \
				$(shell python3-config --includes)

SOURCEDIR = ./src

DFLAGS      = -DPYBIND11_ENABLED -D_GLIBCXX_USE_CXX11_ABI=0
CPPSOURCES = $(shell find $(SOURCEDIR) -name '*.cpp')
CSOURCES = $(shell find ./src/opm -name '*.c')
CPPOBJECTS = $(CPPSOURCES:%.cpp=%.o)
COBJECTS = $(CSOURCES:%.c=%.o)

EXECUTABLE = ./opmcpg/_cpggrid.so

BUILD_MACHINE := $(shell whoami)@$(shell hostname)
BUILD_DATE := $(shell TZ=Europe/Amsterdam date +"%d/%m/%Y, %H:%M:%S")
BUILD_GIT_HASH := $(shell git describe --always --dirty --match 'NOT A TAG')

all: $(EXECUTABLE) 

debug: CFLAGS += -O0 -g -fsanitize=address -fno-omit-frame-pointer
debug: all

release: CFLAGS += -O2
release: all

$(EXECUTABLE) : $(CPPOBJECTS) $(COBJECTS)
	$(CXX) -shared -o $@ $(COBJECTS) $(CPPOBJECTS) $(LFLAGS) 

%.o: %.cpp
	$(CXX) $(DFLAGS) $(CXXFLAGS) $(INCLUDEFLAGS) -c $< -o $@

%.o: %.c
	$(CC) $(CFLAGS) $(INCLUDEFLAGS) -c $< -o $@

clean:
	rm -f $(COBJECTS) $(CPPOBJECTS) $(EXECUTABLE)


#.PHONY: build_info

