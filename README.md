[![pypi](https://img.shields.io/pypi/v/opmcpg.svg?colorB=blue)](https://pypi.org/project/opmcpg/#description)

The code in folder src\opm was taken from https://opm-project.org/ in particular https://github.com/OPM/opm-grid/tree/master/opm/grid
and upgraded to C++ using std::vector instead of pointers ans mallocto be able to expose them to python. Added pybind11.
This wheel contains only dynamic library which could be imported in python