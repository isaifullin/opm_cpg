#!/bin/bash

# Exit when any command fails
set -e
# ------------------------------------------------------------------------------

#echo -e "\n- Update submodules: START\n"
git submodule update --recursive --init

#
rm -rf ./dist
rm -rf build
if ls opmcpg/*.so 1> /dev/null 2>&1; then
    rm opmcpg/*.so
fi

mkdir build
cd build
cmake -D CMAKE_INSTALL_PREFIX=./ ..

# Build
make install

# create a wheel (optional)
cd ..
python3 setup.py build bdist_wheel

# installing python package
python3 -m pip install .

