git submodule update --recursive --init || goto :error

rmdir /s /q build 2> NUL
rmdir /s /q dist 2> NUL
del opmcpg\*.pyd 2> NUL

mkdir build
cd build
cmake -D CMAKE_INSTALL_PREFIX=./ ..

msbuild INSTALL.vcxproj /p:Configuration=Release /p:Platform=x64 -maxCpuCount:2 || goto :error
cd ..

rem make sure wheel package could be installed
python -m pip install wheel 
python -m pip install build 

rem Build wheel
python setup.py build bdist_wheel

rem install python package
pip install .

rem || goto :error checks exit code of command 
rem if one of the commands fails, interrupt batch and return error code
:error
echo Build finished with error code %errorlevel%.
exit /b %errorlevel%

