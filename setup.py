from setuptools import setup, find_packages, Distribution
import os

# custom class to inform setuptools about self-comiled extensions in the distribution
# and hence enforce it to create platform wheel
class BinaryDistribution(Distribution):
    def has_ext_modules(foo):
        return True

here = os.path.abspath(os.path.dirname(__file__))

setup(
    # Add package opmcpg (only)
    packages = ['opmcpg'],

    # Now only include already built libraries
    package_data={'opmcpg': ['*.pyd', '*.so', '*.dll']},

    # Package metadata
    description = "Corner point grid processing",

    # Dependent packages (distributions)
    install_requires=['numpy'],

    classifiers = [
  	"License :: OSI Approved :: GNU General Public License v3 (GPLv3)",

  	"Programming Language :: Python :: 3",
  	"Programming Language :: Python :: 3.7",
 	"Programming Language :: Python :: 3.8",
  	"Programming Language :: Python :: 3.9",
 	"Programming Language :: Python :: 3.10",
  	"Programming Language :: Python :: 3.11",
  	"Programming Language :: Python :: 3 :: Only",
	],

    distclass=BinaryDistribution,
)

