#include "main.h"

UnstructuredGrid
process_cpg_grid(std::vector<int>& dims, std::vector<double>& coord,
	std::vector<double>& zcorn, std::vector<int>& actnum,
	double minpv, std::string result_fname)
{
	const double z_tolerance = 0.0; // OPM's parameter. node coincidence
	const double pinch_value = 0.0; // OPM's parameter. pinch gap, used in minpv process.
	// makes sense only if 6-th param of mp.process is 'false'

	int nx = static_cast<int>(dims[0]);
	int ny = static_cast<int>(dims[1]);
	int nz = static_cast<int>(dims[2]);

	struct grdecl g;

	g.dims[0] = nx;
	g.dims[1] = ny;
	g.dims[2] = nz;
	g.coord = coord.data();
	g.zcorn = zcorn.data();
	g.actnum = actnum.data();

	if (minpv > 0) {
		// filter small volumes
		//TODO
		//cpg_filter_small_volumes(g, coord, zcorn, actnum, nx, ny, nz,
		//	minpv, pinch_value, z_tolerance,
		//	result_fname);
	}

	return create_grid_cornerpoint(&g, z_tolerance);
}
