#pragma once


#include <opm/grid/cornerpoint_grid.h>
#include <opm/grid/UnstructuredGrid.h>
#include "opm/grid/cpgpreprocess/preprocess.h"

#include <map>
#include <set>
#include <string>
#include <array>

UnstructuredGrid process_cpg_grid(std::vector<int>& dims, std::vector<double>& coord,
	std::vector<double>& zcorn, std::vector<int>& actnum,
	double minpv, std::string result_fname);