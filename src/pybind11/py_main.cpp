#include "py_global.h"
#include "cornerpoint_grid.h"
#include "main.h"

namespace py = pybind11;

void pybind_opm_cpg(py::module &m);

UnstructuredGrid
process_cpg_grid(std::vector<int>& dims, std::vector<double>& coord,
	std::vector<double>& zcorn, std::vector<int>& actnum,
	double minpv, std::string result_fname);


PYBIND11_MODULE(_cpggrid, m)
{
	py::bind_vector<std::vector<index_t>>(m, "index_vector", py::module_local(), py::buffer_protocol());
	py::bind_vector<std::vector<value_t>>(m, "value_vector", py::module_local(), py::buffer_protocol())
		.def(py::pickle(
			[](const std::vector<value_t> &p) { // __getstate__
		py::tuple t(p.size());
		for (int i = 0; i < p.size(); i++)
			t[i] = p[i];

		return t;
	},
			[](py::tuple t) { // __setstate__
		std::vector<value_t> p(t.size());

		for (int i = 0; i < p.size(); i++)
			p[i] = t[i].cast<value_t>();

		return p;
	}));
	
	m.def("create_grid_cornerpoint", create_grid_cornerpoint);
	m.def("process_cpg_grid", process_cpg_grid);

	pybind_opm_cpg(m);
}

void pybind_opm_cpg(py::module& m)
{
  //UnstructuredGrid
	py::class_<UnstructuredGrid>(m, "UnstructuredGrid", py::module_local())
		.def(py::init<>())
		.def_readwrite("dimensions", &UnstructuredGrid::dimensions)
		.def_readwrite("number_of_cells", &UnstructuredGrid::number_of_cells)
		.def_readwrite("number_of_faces", &UnstructuredGrid::number_of_faces)
		.def_readwrite("number_of_nodes", &UnstructuredGrid::number_of_nodes)
		.def_readwrite("global_cell", &UnstructuredGrid::global_cell)
		.def_readwrite("face_nodes", &UnstructuredGrid::face_nodes)
		.def_readwrite("face_nodepos", &UnstructuredGrid::face_nodepos)
		.def_readwrite("face_cells", &UnstructuredGrid::face_cells)
		.def_readwrite("cell_faces", &UnstructuredGrid::cell_faces)
		.def_readwrite("cell_facepos", &UnstructuredGrid::cell_facepos)
		.def_readwrite("cell_facetag", &UnstructuredGrid::cell_facetag)
		.def_readwrite("node_coordinates", &UnstructuredGrid::node_coordinates)
		.def_readwrite("face_centroids", &UnstructuredGrid::face_centroids)
		.def_readwrite("face_areas", &UnstructuredGrid::face_areas)
		.def_readwrite("face_normals", &UnstructuredGrid::face_normals)
		.def_readwrite("cell_centroids", &UnstructuredGrid::cell_centroids)
		.def_readwrite("cell_volumes", &UnstructuredGrid::cell_volumes)
		;
}

